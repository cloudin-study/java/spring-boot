
/* Drop Tables */

DROP TABLE IF EXISTS class_room;
DROP TABLE IF EXISTS tutorial_class;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS tutorial_group;
DROP TABLE IF EXISTS teacher;




/* Create Tables */

CREATE TABLE class_room
(
	class_room_id int NOT NULL,
	class_room_name varchar(20),
	capancity int,
	PRIMARY KEY (class_room_id)
) WITHOUT OIDS;


CREATE TABLE student
(
	student_id varchar(10) NOT NULL,
	student_name varchar(10),
	date_of_birth date,
	gender varchar(1) NOT NULL,
	PRIMARY KEY (student_id)
) WITHOUT OIDS;


CREATE TABLE teacher
(
	teacher_id varchar(10) NOT NULL,
	teacher_name varchar(10),
	date_of_birth date,
	gender varchar(1) NOT NULL,
	PRIMARY KEY (teacher_id)
) WITHOUT OIDS;


CREATE TABLE tutorial_class
(
	tutorial_class_id serial NOT NULL,
	group_id varchar(10) NOT NULL,
	student_id varchar(10) NOT NULL,
	PRIMARY KEY (tutorial_class_id)
) WITHOUT OIDS;


CREATE TABLE tutorial_group
(
	group_id varchar(10) NOT NULL,
	group_name varchar(10),
	teacher_id varchar(10) NOT NULL,
	PRIMARY KEY (group_id)
) WITHOUT OIDS;



/* Create Foreign Keys */

ALTER TABLE tutorial_class
	ADD FOREIGN KEY (student_id)
	REFERENCES student (student_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE tutorial_group
	ADD FOREIGN KEY (teacher_id)
	REFERENCES teacher (teacher_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE tutorial_class
	ADD FOREIGN KEY (group_id)
	REFERENCES tutorial_group (group_id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



