package jp.co.student.springbootstudy.vo;

import lombok.Data;

@Data
public class TeacherSearchVO {

	private String teacherId;
	
	private String teacherName;
	
	private int age;
	
	private String gender;
	
}
