package jp.co.student.springbootstudy.inmemory.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import jp.co.student.springbootstudy.vo.StudentVO;

@Component
public class DummyStudentStorage {

	private Map<String, StudentVO> studentMap = new HashMap<String, StudentVO>();

	public int saveStudent(StudentVO studentVO) {
		this.studentMap.put(studentVO.getStudentId(), studentVO);
		return 1;
	}

	public StudentVO getStudent(String studentId) {

		StudentVO studentNotFound = new StudentVO();
		studentNotFound.setStudentId(studentId);
		studentNotFound.setStudentName("学生が存在しません！");

		StudentVO retVO = this.studentMap.getOrDefault(studentId, studentNotFound);

		return retVO;
	}

	public int deleteStudent(String studentId) {
		this.studentMap.remove(studentId);
		return 1;
	}


	public int updateStudent(StudentVO studentVO) {
		StudentVO sv = this.studentMap.get(studentVO.getStudentId());
		if (null == sv) {
			return 0;
		}
		sv.updateStudent(studentVO);
		return 1;
	}

	public List<StudentVO> getAllStudent() {

		List<StudentVO> allStudent = new ArrayList<>(studentMap.values());

		return allStudent;
	}

}
