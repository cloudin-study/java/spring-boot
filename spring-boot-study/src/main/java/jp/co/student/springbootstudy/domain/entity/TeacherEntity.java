package jp.co.student.springbootstudy.domain.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TeacherMapper;
import jp.co.student.springbootstudy.domain.model.Teacher;
import jp.co.student.springbootstudy.domain.model.TeacherExample;
import jp.co.student.springbootstudy.vo.TeacherVO;


@Entity
public class TeacherEntity {
	@Autowired
	private TeacherMapper teacherMapper;
	
	public int insertTeacher(Teacher record) {
		return teacherMapper.insertSelective(record);
	}
	
	public int updateTeacher(Teacher record) {
		return teacherMapper.updateByPrimaryKey(record);
	}
	public List<TeacherVO> getAllTeacher(){
		TeacherExample example = new TeacherExample();
		example.setOrderByClause("teacher_id");
		List<Teacher> teacherList = teacherMapper.selectByExample(example);
		List<TeacherVO> teacherVOList = new ArrayList<TeacherVO>();
		teacherList.forEach(teacher->{
			teacherVOList.add(TeacherVO.fromTeacher(teacher));
		});
		return teacherVOList;
	}
	
	public TeacherVO getTeacher(String teacherId) {
		 
		TeacherVO teacherNotFound = new TeacherVO();
		teacherNotFound.setTeacherId(teacherId);
		teacherNotFound.setTeacherName("教师が存在しません！");
		Teacher teacher = teacherMapper.selectByPrimaryKey(teacherId);
		if (null==teacher) {
			return teacherNotFound;
		}
		return TeacherVO.fromTeacher(teacher);
	}
	public int deleteTeacherByTeacherId(String teacherId) {
		return teacherMapper.deleteByPrimaryKey(teacherId);
	}
}
