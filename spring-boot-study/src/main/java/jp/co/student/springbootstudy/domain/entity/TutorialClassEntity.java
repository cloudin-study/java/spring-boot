package jp.co.student.springbootstudy.domain.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TutorialClassMapper;
import jp.co.student.springbootstudy.domain.model.TutorialClass;
import jp.co.student.springbootstudy.domain.model.TutorialClassExample;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

@Entity
public class TutorialClassEntity {
	@Autowired
	private TutorialClassMapper tutorialClassMapper;
	
	public int insertTutorialClass(TutorialClass record) {
		return tutorialClassMapper.insertSelective(record);
	}
	
	public int updateTutorialClass(TutorialClass record) {
		return tutorialClassMapper.updateByPrimaryKey(record);
	}
	public List<TutorialClassVO> getAllTutorialClass(){
		TutorialClassExample example = new TutorialClassExample();
		example.setOrderByClause("Tutorial_Class_Id");
		List<TutorialClass> tutorialClassList = tutorialClassMapper.selectByExample(example);
		List<TutorialClassVO> tutorialClassVOList = new ArrayList<TutorialClassVO>();
		tutorialClassList.forEach(tutorialClass->{
			tutorialClassVOList.add(TutorialClassVO.fromTutorialClass(tutorialClass));
		});
		return tutorialClassVOList;
	}
	
	public TutorialClassVO getTutorialClass(Integer tutorialClassId) {
		
		TutorialClassVO tutorialClassNotFound = new TutorialClassVO();
		tutorialClassNotFound.setTutorialClassId(tutorialClassId);
		tutorialClassNotFound.setStudentId("存在しません");
		TutorialClass tutorialClass = tutorialClassMapper.selectByPrimaryKey(tutorialClassId);
		if(null==tutorialClass) {
			return tutorialClassNotFound;
		}
		return TutorialClassVO.fromTutorialClass(tutorialClass);
	}
	
	public int deleteTutorialClassByTutorialClassId(Integer tutorialClassId) {
		return tutorialClassMapper.deleteByPrimaryKey(tutorialClassId);
	}
	
}
