package jp.co.student.springbootstudy.service;

import java.util.List;

import jp.co.student.springbootstudy.vo.TeacherSearchVO;
import jp.co.student.springbootstudy.vo.TeacherVO;

public interface TeacherService {

	int saveTeacher(TeacherVO teacherVO);

	int updateTeacher(TeacherVO teacherVO);

	TeacherVO getTeacher(String teacherId);
	
	int deleteTeacherByTeacherId(String teacherId);

	List<TeacherVO> getAllTeacher();
	
	List<TeacherVO> searchByCondition(TeacherVO teacherVO);

}
