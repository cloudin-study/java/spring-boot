package jp.co.student.springbootstudy.vo;

import lombok.Data;

@Data
public class TutorialClassSearchVO {

	
	private Integer tutorialClassId;
	
	private String studentId;
	
	private String groupId;
	
	private String studentName;
	
	private int age;
	
	private String gender;
	
	private String teacherId;
	
	private String teacherName;
	
	private int teacherAge;
	
	private String teacherGender;
	
	private String groupName;


}
