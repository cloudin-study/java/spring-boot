package jp.co.student.springbootstudy.vo;

import java.util.Date;

import lombok.Data;

@Data
public class StudentSearchVO {
	
	private String studentId;

	private String studentName;

	private Date dateOfBirth;

	private String gender;
}
