package jp.co.student.springbootstudy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.student.springbootstudy.domain.entity.GroupEntity;
import jp.co.student.springbootstudy.domain.entity.GroupEntityExt;
import jp.co.student.springbootstudy.domain.model.TutorialGroup;
import jp.co.student.springbootstudy.service.GroupService;
import jp.co.student.springbootstudy.vo.TutorialGroupVO;

@Service
public class GroupServicelmpl implements GroupService{
	
	@Autowired
	private GroupEntity groupEntity;
	@Autowired
	private GroupEntityExt groupEntityExt;
	
	public int insertGroup(TutorialGroupVO groupVO) {
	    TutorialGroup group = groupVO.toTutorialGroup();
	    return groupEntity.insertTutorialGroup(group);
	    
	}
	
	@Override
	public int saveGroup(TutorialGroupVO groupVO) {
		TutorialGroup group = groupVO.toTutorialGroup();
		return groupEntity.insertTutorialGroup(group);
	}

	@Override
	public TutorialGroupVO getGroup(String groupId) {
		return this.groupEntity.getGroup(groupId);
	}

	@Override
	public int updateTutorialGroup(TutorialGroupVO tutorialgroupVO) {
		TutorialGroup tutorialGroup = tutorialgroupVO.toTutorialGroup();
		return groupEntity.updateTutorialGroup(tutorialGroup);
	}

	@Override
	public List<TutorialGroupVO> getAllTutorialGroup(TutorialGroupVO tutorialGroupVO) {
		return  groupEntity.getAllTutorialGroup(tutorialGroupVO);
	}

	@Override
	public int deleteTutorialGroupByGroupId(String groupId) {
		return groupEntity.deleteTutorialGroupByGroupId(groupId);
	}

	@Override
	public List<TutorialGroupVO> searchByCondition(TutorialGroupVO tutorialGroupVO) {
		return groupEntityExt.searchByCondition(tutorialGroupVO);
	}


}
