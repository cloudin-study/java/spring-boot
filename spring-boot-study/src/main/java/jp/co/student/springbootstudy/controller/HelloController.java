package jp.co.student.springbootstudy.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/hello")
public class HelloController {

	@GetMapping("/world")
	public String index() {
		return "Hello world ! Greetings from Spring Boot!";
	}

	
	@GetMapping("/yuan")
	public String helloyuanyuan() {
		return "Hello YuanYuan!";
	}
	
	@GetMapping("/to/{name}")
	public String sayHelloTo(@PathVariable String name) {
		return "Hello " + name + "!";
	}

}