package jp.co.student.springbootstudy.inmemory.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import jp.co.student.springbootstudy.vo.TeacherVO;

@Component

public class DummyTeacherStorage {
	private Map<String, TeacherVO> teacherMap = new HashMap<String, TeacherVO>();

	public int saveTeacher(TeacherVO teacherVO) {
		this.teacherMap.put(teacherVO.getTeacherId(), teacherVO);
		return 1;
	}

	public TeacherVO getTeacher(String teacherId) {

		TeacherVO teacherNotFound = new TeacherVO();
		teacherNotFound.setTeacherId(teacherId);
		teacherNotFound.setTeacherName("先生が存在しません！");

		TeacherVO retVO = this.teacherMap.getOrDefault(teacherId, teacherNotFound);

		return retVO;
	}

	public int deleteTeacher(String teacherId) {
		this.teacherMap.remove(teacherId);
		return 1;
	}

	public int updateTeacher(TeacherVO teacherVO) {
		TeacherVO tv = this.teacherMap.get(teacherVO.getTeacherId());

		if (null == tv) {
			return 0;
		}

		tv.updateTeacher(teacherVO);
		return 1;
	}

	public List<TeacherVO> getAllTeacher() {

		List<TeacherVO> allTeacher = new ArrayList<>(teacherMap.values());

		return allTeacher;
	}

}
