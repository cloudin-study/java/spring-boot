package jp.co.student.springbootstudy.vo;

import org.springframework.beans.BeanUtils;

import jp.co.student.springbootstudy.domain.model.TutorialGroup;
import lombok.Data;

@Data
public class TutorialGroupVO {
	private String groupId;
	private String groupName;
	private String teacherId;
	private String teacherName;


	public void update(TutorialGroupVO target) {

		if (null != target.getGroupId()) {
			this.groupId = target.getGroupId();
		}

		if (null != target.getTeacherId()) {
			this.teacherId = target.getTeacherId();
		}

		if (null != target.getGroupName()) {
			this.groupName = target.getGroupName();
		}

	}

	public boolean groupIdis(String groupId) {
		return this.groupId.equals(groupId);
	}

	public boolean teacherIdis(String teacherId) {
		return this.teacherId.equals(teacherId);
	}
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.lineSeparator());
		sb.append("Group ID:");
		sb.append(this.teacherId);
		sb.append(System.lineSeparator());

		sb.append("Group Name:");
		sb.append(this.groupName);
		sb.append(System.lineSeparator());


		sb.append("Teacher ID:");
		sb.append(this.teacherId);
		sb.append(System.lineSeparator());

		return sb.toString();

	}
	
	public TutorialGroup toTutorialGroup() {
		TutorialGroup group=new TutorialGroup();
		BeanUtils.copyProperties(this, group);
		return group;
		
	}
	public static TutorialGroupVO fromTutorialGroup(TutorialGroupVO group) {
		TutorialGroupVO vo=new TutorialGroupVO();
		BeanUtils.copyProperties(group, vo);
		return vo;
		
	}

}
