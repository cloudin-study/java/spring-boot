package jp.co.student.springbootstudy.service;
import java.util.List;

import jp.co.student.springbootstudy.vo.TutorialGroupVO;

public interface GroupService {
	
	int saveGroup(TutorialGroupVO groupVO);
	
	int updateTutorialGroup(TutorialGroupVO tutorialgroupVO);
	
	TutorialGroupVO getGroup(String groupId);
	
	int deleteTutorialGroupByGroupId(String groupId);
	
	List<TutorialGroupVO> getAllTutorialGroup(TutorialGroupVO tutorialGroupVO);
	
	List<TutorialGroupVO> searchByCondition(TutorialGroupVO tutorialGroupVO);

	
}
