package jp.co.student.springbootstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.inmemory.dummy.DummyTeacherStorage;
import jp.co.student.springbootstudy.vo.TeacherVO;

@RestController
@CrossOrigin
@RequestMapping("/teacher")
public class TeatherController {
	@Autowired
	private DummyTeacherStorage dummyTeacherStorage;

	@PostMapping("/saveTeacher")
	public int saveTeacher(@RequestBody TeacherVO teacherVO) {

		int returnVal = this.dummyTeacherStorage.saveTeacher(teacherVO);
		System.out.println(teacherVO.toString());

		System.out.println(teacherVO.getTeacherName() + "を保存しました。");
		return returnVal;
	}

	@PostMapping("/updateTeacher")
	public int update(@RequestBody TeacherVO teacherVO) {
		int returnVal = this.dummyTeacherStorage.updateTeacher(teacherVO);
		System.out.println(teacherVO.toString());

		System.out.println(teacherVO.getTeacherName() + "");
		return returnVal;
	}

	@PostMapping("/deleteTeacher/{teacherId}")
	public int delete(@PathVariable String teacherId) {
		int returnVal = this.dummyTeacherStorage.deleteTeacher(teacherId);

		return returnVal;
	}

	@PostMapping("/deleteTeacherFromBody")
	public int deleteTeacherFromBody(@RequestBody TeacherVO teacher) {
		int returnVal = this.dummyTeacherStorage.deleteTeacher(teacher.getTeacherId());

		return returnVal;
	}

	@GetMapping("/getTeacher/{teacherId}")
	public TeacherVO getTeacher(@PathVariable String teacherId) {

		return this.dummyTeacherStorage.getTeacher(teacherId);

	}

	@GetMapping("/getAllTeacher")
	public List<TeacherVO> getAllTeacher() {
		return this.dummyTeacherStorage.getAllTeacher();
	}

}
