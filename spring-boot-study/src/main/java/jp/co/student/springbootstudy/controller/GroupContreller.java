package jp.co.student.springbootstudy.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.service.GroupService;
//import jp.co.student.springbootstudy.service.TeacherService;
//import jp.co.student.springbootstudy.vo.TeacherVO;
import jp.co.student.springbootstudy.vo.TutorialGroupVO;



@RestController
@CrossOrigin
@RequestMapping("/group")
public class GroupContreller {
//	@Autowired
//    private StudentService studentService;
//	@Autowired
//	private TeacherService teacherService;
	@Autowired
	private GroupService groupService;
//	private TeacherService teacherService;
	
	@PostMapping("/saveGroup")
	public int saveGroup(@RequestBody TutorialGroupVO groupVO) {
		return groupService.saveGroup(groupVO);
		
	}
//	@PostMapping("/saveTeacher")
//	public int saveTeacher(@RequestBody TeacherVO teacherVO) {
//		return teacherService.saveTeacher(teacherVO);
//		
//	}
	@GetMapping("/getGroup/{groupId}")
	public TutorialGroupVO getGroupVO(@PathVariable String groupId) {
		return groupService.getGroup(groupId);
	}
	@GetMapping("/getAllGroup")
	public List<TutorialGroupVO> getAllGroup(TutorialGroupVO tutorialGroupVO){
		return groupService.getAllTutorialGroup(tutorialGroupVO);
	}
	
	@PostMapping("/updateGroup")
	public int updateTutorialGroup(@RequestBody TutorialGroupVO groupVO) {
		return groupService.updateTutorialGroup(groupVO);
	}
	
	@PostMapping("/deletTutorialGroupByGroupId/{groupId}")
	public int deleteTutorialGroupByGroupId(@PathVariable String groupId) {
		return groupService.deleteTutorialGroupByGroupId(groupId);
	}
	
	
	@PostMapping("/searchByCondition")
	public List<TutorialGroupVO> searchByCondition(@RequestBody TutorialGroupVO tutorialGroupVO){
	return groupService.searchByCondition(tutorialGroupVO);
 }
}
