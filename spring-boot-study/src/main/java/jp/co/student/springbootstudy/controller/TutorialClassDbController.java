package jp.co.student.springbootstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.service.TutorialClassService;
import jp.co.student.springbootstudy.vo.TutorialClassSearchVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

@RestController
@CrossOrigin
@RequestMapping("/tutorial-class-db")
public class TutorialClassDbController {

	@Autowired
	private TutorialClassService tutorialClassService;
	
	@PostMapping("/saveTutorialClass")
	public int saveTutorialClass(@RequestBody TutorialClassVO tutorialClassVO) {
		return tutorialClassService.saveTutorialClass(tutorialClassVO);
	}
	
	@GetMapping("/getTutorialClass/{tutorialClassId}")
	public TutorialClassVO getTutorialClass(@PathVariable Integer tutorialClassId) {
		return tutorialClassService.getTutorialClass(tutorialClassId);
	}
	
	@GetMapping("/getAllTutorialClass")
	public List<TutorialClassVO> getAllTutorialClass() {    
	    return tutorialClassService.getAllTutroialClass();
	}
	
	@PostMapping("/updateTutorialClass")
	public int updateTutorialClass(@RequestBody TutorialClassVO tutorialClass) {
		return tutorialClassService.updateTutorialClass(tutorialClass);
	}
	@PostMapping("/deletTuorialClassByTuorialClassId/{tutorialClassId}")
	public int deletrTutorialClassByTutorialClassId(@PathVariable Integer tutorialClassId) {
		return tutorialClassService.deleteTutroialClassByTutroialClassId(tutorialClassId);
	}
	
	@PostMapping(value = "/searchByCondition")
	public List<TutorialClassVO> searchByCondition(@RequestBody TutorialClassSearchVO tutorialClassSearchVO){
		return tutorialClassService.searchByCondition(tutorialClassSearchVO);
	}

			
}
