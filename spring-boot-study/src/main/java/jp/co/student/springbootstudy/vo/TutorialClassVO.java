package jp.co.student.springbootstudy.vo;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

import jp.co.student.springbootstudy.domain.model.TutorialClass;
import lombok.Data;

@Data
public class TutorialClassVO {

	private Integer tutorialClassId;
	private String studentId;
	private String gender;
	private String studentName;
	private int age;
	
	private String groupId;
	private String groupName;
	
	private String teacherId;
	private String teacherName;
	private String teacherGender;
	private String teacherAge;
	
	
	
	public void updateTutorialClass(TutorialClassVO target) {
		
		if(!ObjectUtils.isEmpty(target.getStudentId()))  {
			this.studentId = target.getStudentId();
		}
		if(!ObjectUtils.isEmpty(target.getGroupId()))  {
			this.studentId = target.getGroupId();
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.lineSeparator());
		sb.append("Tutorial Class Id:");
		sb.append(this.tutorialClassId);
		sb.append(System.lineSeparator());
		
		sb.append("Group Id:");
		sb.append(this.groupId);
		sb.append(System.lineSeparator());
		
		sb.append("Srudent Id:");
		sb.append(this.studentId);
		sb.append(System.lineSeparator());
		
		sb.append("Srudent Name:");
		sb.append(this.studentName);
		sb.append(System.lineSeparator());
		
		sb.append("Age:");
		sb.append(this.age);
		sb.append(System.lineSeparator());
		
		
		return sb.toString();
		
	}
	
	public TutorialClass toTutouialClass() {
		TutorialClass tutorialClass = new TutorialClass();
		BeanUtils.copyProperties(this,tutorialClass);	
		return tutorialClass;
	}
	
	public static TutorialClassVO fromTutorialClass(TutorialClass tutorialClass) {
		TutorialClassVO vo = new TutorialClassVO();
		BeanUtils.copyProperties(tutorialClass, vo);
		return vo;
	}
	

	
}
