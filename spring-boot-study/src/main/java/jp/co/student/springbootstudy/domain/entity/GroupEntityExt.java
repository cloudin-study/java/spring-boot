package jp.co.student.springbootstudy.domain.entity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TutorialGroupEntityMapper;
import jp.co.student.springbootstudy.vo.TutorialGroupVO;

@Entity
public class GroupEntityExt {

	@Autowired
	private TutorialGroupEntityMapper entityMapper;
	 public List<TutorialGroupVO> searchByCondition(TutorialGroupVO tutorialGroupVO){
	 	return this.entityMapper.searchByCondition(tutorialGroupVO);
	}
}