package jp.co.student.springbootstudy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.student.springbootstudy.domain.entity.TutorialClassEntity;
import jp.co.student.springbootstudy.domain.entity.TutorialClassEntityExt;
import jp.co.student.springbootstudy.domain.model.TutorialClass;
import jp.co.student.springbootstudy.service.TutorialClassService;
import jp.co.student.springbootstudy.vo.TutorialClassSearchVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

@Service
public class TutorialClassServiceImpl implements TutorialClassService{
	
	@Autowired
	private TutorialClassEntity tutorialClassEntity;
	@Autowired
	private TutorialClassEntityExt tutorialClassEntityExt;
	
	
	public int insertTutorialClass(TutorialClassVO tutorialClassVO) {
		TutorialClass tutorialClass = tutorialClassVO.toTutouialClass();
		return tutorialClassEntity.insertTutorialClass(tutorialClass);
	}
	
	@Override
	public TutorialClassVO getTutorialClass(Integer tutorialClassId) {
		return tutorialClassEntity.getTutorialClass(tutorialClassId);
	}

	@Override
	public int saveTutorialClass(TutorialClassVO tutorialClassVO) {
		TutorialClass tutorialClass = tutorialClassVO.toTutouialClass();
		return tutorialClassEntity.insertTutorialClass(tutorialClass);
	}

	@Override
	public int updateTutorialClass(TutorialClassVO tutroialClassVO) {
		TutorialClass tutroialClass = tutroialClassVO.toTutouialClass();
		return tutorialClassEntity.updateTutorialClass(tutroialClass);
	}

	@Override
	public int deleteTutroialClassByTutroialClassId(Integer tutroialClassId) {
		return tutorialClassEntity.deleteTutorialClassByTutorialClassId(tutroialClassId);
	}

	@Override
	public List<TutorialClassVO> getAllTutroialClass() {
		return tutorialClassEntity.getAllTutorialClass();
	}

	@Override
	public List<TutorialClassVO> searchByCondition(TutorialClassSearchVO tutorialClassSearchVO) {
		
		
		return tutorialClassEntityExt.searchByCondition(tutorialClassSearchVO);
		
	}


}
