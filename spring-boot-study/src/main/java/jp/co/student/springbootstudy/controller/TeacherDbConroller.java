package jp.co.student.springbootstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.service.TeacherService;
import jp.co.student.springbootstudy.vo.TeacherSearchVO;
import jp.co.student.springbootstudy.vo.TeacherVO;

@RestController
@CrossOrigin
@RequestMapping("/teacher-db")
public class TeacherDbConroller {
	
	
	@Autowired
	private TeacherService teacherService;
	
	@PostMapping("/saveTeacher")
	public int saveTeacher(@RequestBody TeacherVO teacherVO) {
		return teacherService.saveTeacher(teacherVO);
		
	}
	
	@GetMapping("/getTeacher/{teacherId}")
	public TeacherVO getTeacher(@PathVariable String teacherId) {
		return teacherService.getTeacher(teacherId);
		
	}
	
	@GetMapping("/getAllTeacher")
	public List<TeacherVO> getAllTeacher(){
		return teacherService.getAllTeacher();
	}
	
	@PostMapping("/updateTeacher")
	public int updateTeacher(@RequestBody TeacherVO teacher) {
		return teacherService.updateTeacher(teacher);
	}
	
	@PostMapping("/deleteTeacherByTeacherId/{teacherId}")
	public int deleteTeacherByTeacherId(@PathVariable String teacherId) {
		return teacherService.deleteTeacherByTeacherId(teacherId);
	}
	
	
	@PostMapping("/searchByCondition")
	public List<TeacherVO> searchByCondition(@RequestBody TeacherVO teacherVO){
		return teacherService.searchByCondition(teacherVO);	
	}
}
