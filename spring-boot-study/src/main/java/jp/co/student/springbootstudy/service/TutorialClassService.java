package jp.co.student.springbootstudy.service;

import java.util.List;

import jp.co.student.springbootstudy.vo.TutorialClassSearchVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

public interface TutorialClassService {

	int saveTutorialClass(TutorialClassVO tutorialClassVO);
	
	int updateTutorialClass(TutorialClassVO tutroialClassVO);

	TutorialClassVO getTutorialClass(Integer tutorialClassId);
	
	int deleteTutroialClassByTutroialClassId(Integer tutroialClassId);
	
	List<TutorialClassVO> getAllTutroialClass();
	
	List<TutorialClassVO> searchByCondition(TutorialClassSearchVO tutorialClassSearchVO);
	
}
