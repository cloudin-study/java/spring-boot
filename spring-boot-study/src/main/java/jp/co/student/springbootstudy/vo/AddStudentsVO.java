package jp.co.student.springbootstudy.vo;

import java.util.List;

import lombok.Data;

@Data
public class AddStudentsVO {
	
	private List<String> studentIds;
	
    private String groupId;
}
