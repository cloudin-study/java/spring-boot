package jp.co.student.springbootstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.service.StudentService;
import jp.co.student.springbootstudy.service.TutorialClassService;
import jp.co.student.springbootstudy.vo.AddStudentsVO;
import jp.co.student.springbootstudy.vo.StudentVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

@RestController
@CrossOrigin
@RequestMapping("/student-db")
public class StudentDbConroller {

	@Autowired
	private StudentService studentService;
	@Autowired
	private TutorialClassService tutorialClassService;

	@PostMapping("/saveStudent")
	public int saveStudent(@RequestBody StudentVO studentVO) {
		return studentService.saveStudent(studentVO);
	}

	@GetMapping("/getStudent/{studentId}")
	public StudentVO getStudent(@PathVariable String studentId) {
		return studentService.getStudent(studentId);
	}

	@GetMapping("/getAllStudent")
	public List<StudentVO> getAllStudent() {
		return studentService.getAllStudent();
	}

	@PostMapping("/updateStudent")
	public int updateStudent(@RequestBody StudentVO student) {
		return studentService.updateStudent(student);
	}

	@PostMapping("/deleteStudentByStudentId/{studentId}")
	public int deleteStudentByStudentId(@PathVariable String studentId) {
		return studentService.deleteStudentByStudentId(studentId);

	}

	@PostMapping("/searchByCondition")
	public List<StudentVO> searchByCondition(@RequestBody StudentVO studentVO) {
		return studentService.searchByCondition(studentVO);
	}

	@PostMapping("/addStudentsToClass")
	public void addStudentsToClass(@RequestBody AddStudentsVO addStudentsVO) {
		for (String studentId : addStudentsVO.getStudentIds()) {
			TutorialClassVO tutorialClassVO = new TutorialClassVO();
			tutorialClassVO.setStudentId(studentId);
			tutorialClassVO.setGroupId(addStudentsVO.getGroupId());
			tutorialClassService.saveTutorialClass(tutorialClassVO);
		}
	}

}
