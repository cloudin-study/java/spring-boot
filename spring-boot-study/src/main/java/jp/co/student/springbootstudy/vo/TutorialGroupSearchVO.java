package jp.co.student.springbootstudy.vo;

import lombok.Data;

@Data
public class TutorialGroupSearchVO {
	
	private String groupId;
	private String groupName;
	private String teacherId;

}
