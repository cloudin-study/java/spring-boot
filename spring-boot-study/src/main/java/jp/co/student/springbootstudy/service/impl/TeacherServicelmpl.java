package jp.co.student.springbootstudy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.student.springbootstudy.domain.entity.TeacherEntity;
import jp.co.student.springbootstudy.domain.entity.TeacherEntityExt;
import jp.co.student.springbootstudy.domain.model.Teacher;
import jp.co.student.springbootstudy.service.TeacherService;
import jp.co.student.springbootstudy.vo.TeacherSearchVO;
import jp.co.student.springbootstudy.vo.TeacherVO;

@Service
public class TeacherServicelmpl implements TeacherService{

	@Autowired
	private TeacherEntity teacherEntity;
	@Autowired
	private TeacherEntityExt teacherEntityExt;
	private TeacherVO teacherVO;
	
	
	public int insertTeacher(TeacherVO teacherVO) {
		Teacher teacher = teacherVO.toTeacher();
		return teacherEntity.insertTeacher(teacher);
	}

	@Override
	public int saveTeacher(TeacherVO teacherVO) {
		Teacher teacher = teacherVO.toTeacher();
		return teacherEntity.insertTeacher(teacher);
	}

	@Override
	public int updateTeacher(TeacherVO teacherVO) {
		Teacher teacher = teacherVO.toTeacher();
		return teacherEntity.updateTeacher(teacher);
	}

	@Override
	public TeacherVO getTeacher(String teacherId) {
		return teacherEntity.getTeacher(teacherId);
	}

	@Override
	public List<TeacherVO> getAllTeacher() {
		return teacherEntity.getAllTeacher();
	}

	@Override
	public int deleteTeacherByTeacherId(String teacherId) {
		return teacherEntity.deleteTeacherByTeacherId(teacherId);
	}

	@Override
	public List<TeacherVO> searchByCondition(TeacherVO teacherVO) {
		return teacherEntityExt.searchByCondition(teacherVO);
	}
	
	
	
}
