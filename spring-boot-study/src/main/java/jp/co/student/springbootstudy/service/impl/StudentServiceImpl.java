package jp.co.student.springbootstudy.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.student.springbootstudy.domain.entity.StudentEntity;
import jp.co.student.springbootstudy.domain.entity.StudentEntityExt;
import jp.co.student.springbootstudy.domain.model.Student;
import jp.co.student.springbootstudy.service.StudentService;
import jp.co.student.springbootstudy.vo.StudentSearchVO;
import jp.co.student.springbootstudy.vo.StudentVO;

@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentEntity studentEntity;
	@Autowired
	private StudentEntityExt studentEntityExt;

	public int insertStudent(StudentVO studentVO) {
		Student student = studentVO.toStudent();
		return studentEntity.insertStudent(student);
	}

	@Override
	public int saveStudent(StudentVO studentVO) {
		Student student = studentVO.toStudent();
		return studentEntity.insertStudent(student);
	}

	@Override
	public int updateStudent(StudentVO studentVO) {
		Student student = studentVO.toStudent();
		return studentEntity.updateStudent(student);
	}

	@Override
	public StudentVO getStudent(String studentId) {

		return studentEntity.getStudent(studentId);
	}

	@Override
	public int deleteStudentByStudentId(String studentId) {
		// TODO Auto-generated method stub
		return studentEntity.deleteStudentByStdentId(studentId);
	}

	@Override
	public List<StudentVO> getAllStudent() {
		return studentEntity.getAllStudent();
	}

	@Override
	public List<StudentVO> searchByCondition(StudentVO studentVO) {
		return studentEntityExt.searchByCondition(studentVO);
	};
}
