package jp.co.student.springbootstudy.inmemory.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import jp.co.student.springbootstudy.vo.TutorialGroupVO;


@Component

public class DummyGroupStorage {
	private Map<String, TutorialGroupVO> groupMap = new HashMap<String, TutorialGroupVO>();

	public int saveGroup(TutorialGroupVO groupVO) {
		this.groupMap.put(groupVO.getGroupId(), groupVO);
		return 1;
	}

	public TutorialGroupVO getGroup(String groupId) {

		TutorialGroupVO groupNotFound = new TutorialGroupVO();
		groupNotFound.setGroupId(groupId);
		groupNotFound.setGroupName("グループが存在しません！");
		TutorialGroupVO retVO = this.groupMap.getOrDefault(groupId, groupNotFound);

		return retVO;
	}
	public int deleteGroup(String groupId) {
		this.groupMap.remove(groupId);
		return 1;
	}
	public List<TutorialGroupVO> getAllGroup() {

		List<TutorialGroupVO> allGroup = new ArrayList<>(groupMap.values());

		return allGroup;
	}
	public int updateGroup(TutorialGroupVO groupVO) {
		TutorialGroupVO sv = this.groupMap.get(groupVO.getGroupId());

		if (null == sv) {
			return 0;
		}

		sv.update(groupVO);

		return 1;

	}

	

}
