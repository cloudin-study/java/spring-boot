package jp.co.student.springbootstudy.service;

import java.util.List;

import jp.co.student.springbootstudy.vo.StudentSearchVO;
import jp.co.student.springbootstudy.vo.StudentVO;

public interface StudentService {

	/**  */
	int saveStudent(StudentVO studentVO);

	/**  */
	int updateStudent(StudentVO studentVO);

	StudentVO getStudent(String studentId);

	int deleteStudentByStudentId(String studentId);

	List<StudentVO> getAllStudent();
	
	List<StudentVO> searchByCondition(StudentVO studentVO);
	
	

}
