package jp.co.student.springbootstudy.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jp.co.student.springbootstudy.inmemory.dummy.DummyStudentStorage;
import jp.co.student.springbootstudy.vo.StudentVO;

@RestController
@CrossOrigin
@RequestMapping("/student")
public class StudentController {

	@Autowired
	private DummyStudentStorage dummyStudentStorage;

	@PostMapping("/saveStudent")
	public int saveStudent(@RequestBody StudentVO studentVO) {

		int returnVal = this.dummyStudentStorage.saveStudent(studentVO);
		System.out.println(studentVO.toString());

		System.out.println(studentVO.getStudentName() + "を保存しました。");
		return returnVal;
	}

	@PostMapping("/updateStudent")
	public int update(@RequestBody StudentVO studentVO) {
		int returnVal = this.dummyStudentStorage.updateStudent(studentVO);
		System.out.println(studentVO.toString());

		System.out.println(studentVO.getStudentName() + "");
		return returnVal;
	}

	@PostMapping("/deleteStudent/{studentId}")
	public int delete(@PathVariable String studentId) {
		int returnVal = this.dummyStudentStorage.deleteStudent(studentId);

		return returnVal;
	}
	
	@PostMapping("/deleteStudentFromBody")
	public int deleteStudentFromBody(@RequestBody StudentVO student) {
		int returnVal = this.dummyStudentStorage.deleteStudent(student.getStudentId());
	
		return returnVal;
	}

	@GetMapping("/getStudent/{studentId}")
	public StudentVO getStudent(@PathVariable String studentId) {
		return this.dummyStudentStorage.getStudent(studentId);
	}

	@GetMapping("/getAllStudent")
	public List<StudentVO> getAllStudent() {
		return this.dummyStudentStorage.getAllStudent();
	}

}
