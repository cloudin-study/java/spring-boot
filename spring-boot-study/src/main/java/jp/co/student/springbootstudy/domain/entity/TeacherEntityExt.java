package jp.co.student.springbootstudy.domain.entity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TeacherEntityMapper;
import jp.co.student.springbootstudy.vo.TeacherSearchVO;
import jp.co.student.springbootstudy.vo.TeacherVO;

@Entity
public class TeacherEntityExt {
	
	@Autowired
	private TeacherEntityMapper entityMapper;
		public List<TeacherVO> searchByCondition(TeacherVO teacherVO){
			return this.entityMapper.searchByCondition(teacherVO);
		}
}
