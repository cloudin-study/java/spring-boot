package jp.co.student.springbootstudy.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

import jp.co.student.springbootstudy.domain.model.Student;
import lombok.Data;

@Data
public class StudentVO {

	private String studentId;

	private String studentName;

	private Date dateOfBirth;

	private String gender;

	public void updateStudent(StudentVO target) {

		if (!ObjectUtils.isEmpty(target.getStudentName())) {
			this.studentName = target.getStudentName();
		}

		if (!ObjectUtils.isEmpty(target.getGender())) {

			this.gender = target.getGender();
		}
		

	}

	public boolean studentIdis(String studentId) {
		return this.studentId.equals(studentId);
	}

	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.lineSeparator());
		sb.append("Student ID:");
		sb.append(this.studentId);
		sb.append(System.lineSeparator());

		sb.append("Student Name:");
		sb.append(this.studentName);
		sb.append(System.lineSeparator());

		sb.append(formatDate(dateOfBirth));
		sb.append(this.dateOfBirth);
		sb.append(System.lineSeparator());

		sb.append("Student Gender:");
		sb.append("1".equals(this.gender) ? "男" : "女");
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());
		

		return sb.toString();

	}

	public Student toStudent() {
		Student student = new Student();
		BeanUtils.copyProperties(this, student);
		return student;
	}

	public static StudentVO fromStudent(Student student) {
		StudentVO vo = new StudentVO();
		BeanUtils.copyProperties(student, vo);
		return vo;
	}
	private String formatDate(Date date) {
        if (date == null) {
            return "Unknown";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

}
