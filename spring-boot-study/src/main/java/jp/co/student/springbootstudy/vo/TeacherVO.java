package jp.co.student.springbootstudy.vo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.util.ObjectUtils;

import jp.co.student.springbootstudy.domain.model.Teacher;
import lombok.Data;

@Data
public class TeacherVO {
	private String teacherId;

	private String teacherName;

	private Date dateOfBirth;

	private String gender;

	public void updateTeacher(TeacherVO target) {

		if (!ObjectUtils.isEmpty(target.getTeacherName())) {
			this.teacherName = target.getTeacherName();
		}

		if (null != target.getGender()) {
			this.gender = target.getGender();
		}

	}

	public boolean teacherIdis(String teacherId) {
		return this.teacherId.equals(teacherId);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(System.lineSeparator());
		sb.append("Teacher ID:");
		sb.append(this.teacherId);
		sb.append(System.lineSeparator());

		sb.append("Teacher Name:");
		sb.append(this.teacherName);
		sb.append(System.lineSeparator());

		sb.append(formatDate(dateOfBirth));
		sb.append(this.dateOfBirth);
		sb.append(System.lineSeparator());

		sb.append("Teacher Gender:");
		sb.append("1".equals(this.gender) ? "男" : "女");
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());
		sb.append(System.lineSeparator());

		return sb.toString();

	}

	public Teacher toTeacher() {
		Teacher teacher=new Teacher();
		BeanUtils.copyProperties(this, teacher);
		return teacher;
	}

	public static TeacherVO fromTeacher(Teacher teacher) {
		TeacherVO vo= new TeacherVO();
		BeanUtils.copyProperties(teacher, vo);
		return vo;
	}
	private String formatDate(Date date) {
        if (date == null) {
            return "Unknown";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }
}
