package jp.co.student.springbootstudy.domain.entity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.StudentEntityMapper;
import jp.co.student.springbootstudy.vo.StudentSearchVO;
import jp.co.student.springbootstudy.vo.StudentVO;

@Entity
public class StudentEntityExt {
	
	@Autowired
	private StudentEntityMapper entityMapper;
	 public List<StudentVO> searchByCondition(StudentVO studentVO){
		 return this.entityMapper.searchByCondition(studentVO);
	 }

}
