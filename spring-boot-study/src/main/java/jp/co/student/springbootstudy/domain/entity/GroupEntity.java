package jp.co.student.springbootstudy.domain.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TutorialGroupEntityMapper;
import jp.co.student.springbootstudy.domain.mapper.TutorialGroupMapper;
import jp.co.student.springbootstudy.domain.model.TutorialGroup;
import jp.co.student.springbootstudy.domain.model.TutorialGroupExample;
import jp.co.student.springbootstudy.vo.TutorialGroupVO;

@Entity
public class GroupEntity {
	@Autowired
	private TutorialGroupMapper tutorialGroupMapper;
	@Autowired
	private TutorialGroupEntityMapper tutorialGroupEntityMapper;

	public int insertTutorialGroup(TutorialGroup record) {
		return tutorialGroupMapper.insertSelective(record);
	}

	public int updateTutorialGroup(TutorialGroup record) {
		return tutorialGroupMapper.updateByPrimaryKey(record);
	}

	public List<TutorialGroupVO> getAllTutorialGroup(TutorialGroupVO tutorialGroupVO) {
		TutorialGroupExample example = new TutorialGroupExample();
		example.setOrderByClause("group_id");
		List<TutorialGroupVO> groupList = tutorialGroupEntityMapper.selectGroup(tutorialGroupVO);

		List<TutorialGroupVO> groupVOList = new ArrayList<TutorialGroupVO>();
		groupList.forEach(group -> {
			groupVOList.add(group);
		});

		return groupVOList;
	}

	public TutorialGroupVO getGroup(String groupId) {

		TutorialGroupVO groupNotFound = new TutorialGroupVO();
		groupNotFound.setGroupId(groupId);
		groupNotFound.setGroupName("该班级不存在");
		TutorialGroupVO group = tutorialGroupEntityMapper.selectByPrimaryKey(groupId);
		if (null == group) {
			return groupNotFound;
		}

		return TutorialGroupVO.fromTutorialGroup(group);
	}

	public int deleteTutorialGroupByGroupId(String groupId) {
		return tutorialGroupMapper.deleteByPrimaryKey(groupId);
	}

}
