package jp.co.student.springbootstudy.domain.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.StudentMapper;
import jp.co.student.springbootstudy.domain.model.Student;
import jp.co.student.springbootstudy.domain.model.StudentExample;
import jp.co.student.springbootstudy.vo.StudentVO;

@Entity
public class StudentEntity {
	@Autowired
	private StudentMapper studentMapper;

	public int insertStudent(Student record) {
		return studentMapper.insertSelective(record);
	}

	public int updateStudent(Student record) {
		return studentMapper.updateByPrimaryKeySelective(record);
	}

	public List<StudentVO> getAllStudent() {
		StudentExample example = new StudentExample();
		example.setOrderByClause("student_id");
		List<Student> studentList = studentMapper.selectByExample(example);
		List<StudentVO> studentVOList = new ArrayList<StudentVO>();
		
		studentList.forEach(student->{
			studentVOList.add(StudentVO.fromStudent(student));
		});
		
		return studentVOList;
	}

	public StudentVO getStudent(String studentId) {

		StudentVO studentNotFound = new StudentVO();
		studentNotFound.setStudentId(studentId);
		studentNotFound.setStudentName("学生が存在しません！");
		Student student = studentMapper.selectByPrimaryKey(studentId);
		if (null == student) {
			return studentNotFound;
		}

		return StudentVO.fromStudent(student);
	}
	public int deleteStudentByStdentId(String studentId){
		return studentMapper.deleteByPrimaryKey(studentId);
	}
}