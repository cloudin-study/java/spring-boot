package jp.co.student.springbootstudy.domain.entity;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.student.springbootstudy.domain.mapper.TutorialClassEntityMapper;
import jp.co.student.springbootstudy.vo.TutorialClassSearchVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

@Entity
public class TutorialClassEntityExt {
	
	
	@Autowired
	private TutorialClassEntityMapper entityMapper;
	 public List<TutorialClassVO> searchByCondition(TutorialClassSearchVO tutorialClassSearchVO){
		 return this.entityMapper.searchByCondition(tutorialClassSearchVO);
	}
}
