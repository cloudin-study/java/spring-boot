package jp.co.student.springbootstudy.domain.mapper;

import java.util.List;

import jp.co.student.springbootstudy.vo.StudentSearchVO;
import jp.co.student.springbootstudy.vo.StudentVO;

public interface StudentEntityMapper {
	List<StudentVO> searchByCondition(StudentVO studentVO);
}
