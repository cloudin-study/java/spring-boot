package jp.co.student.springbootstudy.domain.mapper;

import java.util.List;

import jp.co.student.springbootstudy.vo.TutorialGroupVO;



public interface TutorialGroupEntityMapper {

	List<TutorialGroupVO> searchByCondition(TutorialGroupVO tutorialGroupVO);

	List<TutorialGroupVO> selectGroup(TutorialGroupVO tutorialGroupVO);

	TutorialGroupVO selectByPrimaryKey(String groupId);
}
