package jp.co.student.springbootstudy.domain.mapper;

import java.util.List;

import jp.co.student.springbootstudy.vo.TeacherSearchVO;
import jp.co.student.springbootstudy.vo.TeacherVO;

public interface TeacherEntityMapper {

	List<TeacherVO> searchByCondition(TeacherVO teacherVO);
	
}
