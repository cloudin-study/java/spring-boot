package jp.co.student.springbootstudy.domain.mapper;

import java.util.List;

import jp.co.student.springbootstudy.vo.TutorialClassSearchVO;
import jp.co.student.springbootstudy.vo.TutorialClassVO;

public interface TutorialClassEntityMapper {
	List<TutorialClassVO> searchByCondition(TutorialClassSearchVO tutorianClassSearchVO);
}
